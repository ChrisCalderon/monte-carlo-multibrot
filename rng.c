
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#include "rng.h"

#include <stdio.h>

// xoshiro256+ algorithm used here is created by
// David Blackman and Sebastiano Vigna in 2018.


bool rng_init(uint64_t s[static 4]) {
  FILE *random_file = fopen("/dev/random", "r");
  if(random_file == NULL)
    return false;

  fread(&s[0], sizeof(uint64_t), 4, random_file);
  fclose(random_file);
  return true;
}


static inline uint64_t rotl(const uint64_t x, int k) {
  return (x << k) | (x >> (64 - k));
}


double rng_next(uint64_t s[static 4]) {
  const double result = ((s[0] + s[3]) >> 11)*0x1.0p-53;

  const uint64_t t = s[1] << 17;

  s[2] ^= s[0];
  s[3] ^= s[1];
  s[1] ^= s[2];
  s[0] ^= s[3];

  s[2] ^= t;

  s[3] = rotl(s[3], 45);

  return result;
}
