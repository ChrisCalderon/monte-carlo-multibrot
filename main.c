
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#include "pbm.h"
#include "rng.h"
#include "cli.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_POINTS 10000000
#define MAX_ITERATIONS 100
#define IMAGE_WIDTH 2000
#define IMAGE_HEIGHT 2000
#define MIN_REAL -2.0
#define MAX_REAL 2.0
#define MIN_IMAG -2.0
#define MAX_IMAG 2.0
#define POWER 3.0


struct worker_args {
  uint64_t max_points;
  uint64_t max_iterations;
  size_t image_width;
  size_t image_height;
  double min_real;
  double max_real;
  double min_imag;
  double max_imag;
  double power;
};


void *worker(void *args_) {
  struct worker_args *args = args_;
  double max_real = args->max_real;
  double min_real = args->min_real;
  double max_imag = args->max_imag;
  double min_imag = args->min_imag;
  double power = args->power;
  uint64_t rng_state[4];
  rng_init(rng_state);
  
  pbm *worker_image = pbm_create(args->image_width, args->image_height);

  for(uint64_t i = 0; i < args->max_points; i++) {
    double random_real = rng_next(rng_state);
    double random_imag = rng_next(rng_state);

    double c_r = (max_real - min_real)*random_real + min_real;
    double c_i = (max_imag - min_imag)*random_imag + min_imag;
    double z_r = 0.0, z_i = 0.0, temp;
    bool divergent = false;

    for(uint64_t j = 0; j < args->max_iterations; j++) {
      temp = z_r;
      z_r = pow(z_r*z_r + z_i*z_i, power/2.0) * cos(power * atan2(z_i, z_r)) + c_r;
      z_i = pow(temp*temp + z_i*z_i, power/2.0) * sin(power * atan2(z_i, temp)) + c_i;
      divergent |= (z_r*z_r + z_i*z_i) > 4.0;
      if(divergent)
	break;
    }

    if(divergent)
      continue;

    size_t row = args->image_height * (1 - random_imag);
    size_t col = args->image_width * random_real;
    pbm_set_point(worker_image, row, col);
  }

  return worker_image;
}


int main(int argc, char **argv) {

  cli_options options = {
    .max_points = MAX_POINTS,
    .max_iterations = MAX_ITERATIONS,
    .image_height = IMAGE_HEIGHT,
    .image_width = IMAGE_WIDTH,
    .min_real = MIN_REAL,
    .max_real = MAX_REAL,
    .min_imag = MIN_IMAG,
    .max_imag = MAX_IMAG,
    .power = POWER,
    .output = "multibrot.pbm",
  };

  int result = cli_parse(argc, argv, &options);
  switch(result) {
  case -1: // -h option in cli
    return 0;
  case 0: // success
    break;
  default:
    return result;  
  }

  
  long cpu_count = sysconf(_SC_NPROCESSORS_ONLN);
  if(cpu_count == -1) {
    perror("Unable to determine the number of available cores.");
    return EXIT_FAILURE;
  }
  
  pthread_t *threads = calloc(cpu_count, sizeof(pthread_t));
  if(threads == NULL) {
    perror("Unable to allocate space for the threads.");
    return EXIT_FAILURE;
  }

  struct worker_args *all_args = calloc(cpu_count, sizeof(struct worker_args));
  if(all_args == NULL) {
    perror("Unable to allocate space for thread parameters.");
    free(threads);
    return EXIT_FAILURE;
  }

  pbm **image_results = calloc(cpu_count, sizeof(pbm*));
  if(image_results == NULL) {
    perror("Unable to allocate space for thread results.");
    free(all_args);
    free(threads);
    return EXIT_FAILURE;
  }

  for(size_t i=0; i < cpu_count; i++) {
    all_args[i].max_points = options.max_points/cpu_count;
    all_args[i].max_iterations = options.max_iterations;
    all_args[i].image_width = options.image_width;
    all_args[i].image_height = options.image_height;
    all_args[i].min_real = options.min_real;
    all_args[i].max_real = options.max_real;
    all_args[i].min_imag = options.min_imag;
    all_args[i].max_imag = options.max_imag;
    all_args[i].power = options.power;

    if(i == 0)
      all_args[i].max_points += options.max_points % cpu_count;

    pthread_create(threads + i, NULL, worker, all_args + i);
  }

  for(size_t i=0; i < cpu_count; i++) {
    pthread_join(threads[i], (void **)(image_results + i));
  }

  for(size_t i=1; i < cpu_count; i++) {
    for(size_t j=0; j < image_results[0]->data_length; j++) {
      image_results[0]->data[j] |= image_results[i]->data[j];
    }
    pbm_destroy(image_results[i]);
  }
  pbm_save(image_results[0], options.output);
  pbm_destroy(image_results[0]);
  free(threads);
  free(all_args);
  free(image_results);
  return EXIT_SUCCESS;
}
