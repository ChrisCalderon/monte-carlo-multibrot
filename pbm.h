
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#ifndef PBM_H
#define PBM_H

#include <stddef.h>

typedef struct pbm_struct {
  size_t width;
  size_t height;
  size_t row_length;
  size_t data_length;
  char *data; // data is a bit matrix
} pbm;

pbm *pbm_create(size_t width, size_t height);
void pbm_set_point(pbm* image, size_t row, size_t column);
int pbm_save(pbm *image, const char *filename);
void pbm_destroy(pbm *image);

#endif
