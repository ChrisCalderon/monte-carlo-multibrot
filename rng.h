
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#ifndef RNG_H
#define RNG_H

#include <stdint.h>
#include <stdbool.h>

bool rng_init(uint64_t state[static 4]);
double rng_next(uint64_t state[static 4]);

#endif
