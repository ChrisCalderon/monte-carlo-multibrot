SHELL = /opt/local/bin/bash
CC = clang
CFLAGS = -std=c99 -O3 -Wall -Werror -pedantic -c
LDLIBS = -lm -lc
LINK = $(CC) $(LDLIBS)
COMPILE = $(CC) $(CFLAGS)
OBJECTS = main.o pbm.o rng.o cli.o

.PHONY: clean

mcm: $(OBJECTS)
	$(LINK) -o $@ $(OBJECTS) $(LDLIBS)

main.o: main.c *.h
	$(COMPILE) $< -pthread

%.o: %.c %.h
	$(COMPILE) $<

clean:
	if [ -x mcm ]; then rm mcm; fi;
	for file in *.o; do if [ -f $$file ]; then rm $$file; fi; done;
