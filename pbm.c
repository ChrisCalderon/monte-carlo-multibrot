
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#include "pbm.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define MOD_MASK (CHAR_BIT - 1)


pbm *pbm_create(size_t width, size_t height) {
  pbm *image_ptr = malloc(sizeof(pbm));
  if(image_ptr == NULL)
    return NULL;

  size_t row_length = (width/CHAR_BIT) + ((width&MOD_MASK) > 0);
  size_t data_length = height * row_length;
  char *data = calloc(data_length, 1);
  if(data == NULL) {
    free(image_ptr);
    return NULL;
  }

  image_ptr->width = width;
  image_ptr->height = height;
  image_ptr->row_length = row_length;
  image_ptr->data_length = data_length;
  image_ptr->data = data;
  return image_ptr;
}


void pbm_set_point(pbm *image_ptr, size_t row, size_t column) {
  size_t column_index = column/CHAR_BIT;
  size_t column_offset = MOD_MASK - (column&MOD_MASK);
  image_ptr->data[row*image_ptr->row_length + column_index] |= 1 << column_offset;
}


int pbm_save(pbm *image_ptr, const char *filename) {
  FILE *out = fopen(filename, "wb");
  if(out == NULL) {
    return 0;
  }
  size_t rows = image_ptr->width;
  size_t columns = image_ptr->width;
  fprintf(out, "P4\n%zu %zu\n", rows, columns);
  size_t bytes_written = 0;
  while(bytes_written < image_ptr->data_length) {
    bytes_written += fwrite(image_ptr->data + bytes_written,
			    1,
			    image_ptr->data_length - bytes_written,
			    out);
  }
  return 1;
}


void pbm_destroy(pbm *image_ptr) {
  free(image_ptr->data);
  free(image_ptr);
}
