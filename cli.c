
/*
    This file is part of Monte Carlo Multibrot.
    Copyright (C) 2022  Christian Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    You can contact the author via email at calderonchristian73 AT gmail DOT com
*/
#include "cli.h"
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <sysexits.h>

#ifndef PROG
#define PROG "randelbrot"
#endif

#ifndef BASE
// You can redefine this at compile time if you're a disgusting sadist.
#define BASE 10
#endif

#if !((BASE == 0) || ((2 <= BASE) && (BASE <= 36)))
#error "Invalid base for strtoul"
#endif


static void usage() {
  puts(
       "Usage: " PROG " [OPTION]...\n"
       "Generates a black and white mutlibrot image using random points.\n\n"
       "Options:\n"
       "  -h --help          Show this help message and exit.\n"
       "  -o --output        Set the name of the image file.\n"
       "  -W --image-width   Set the pixel width of the image.\n"
       "  -H --image-height  Set the pixel height of the image.\n"
       "  -r --real-min      Set the mininum value of the real part.\n"
       "  -R --real-max      Set the maximum value of the real part.\n"
       "  -i --imag-min      Set the minimum value of the imaginary part.\n"
       "  -I --imag-max      Set the maximum value of the imaginary part.\n"
       "  -p --point-count   Set the number of random points to try.\n"
       "  -t --trials        Set the number of iterations a point must pass.\n"
       "  -P --power         Set the power used in the iteration function.\n"
       );
}


static bool handle_uint64_t(const char *name, char *opt_str, uint64_t *out) {
  char *str_end;
  uint64_t result = strtoul(opt_str, &str_end, BASE);

  if(errno == ERANGE) {
    fprintf(stderr, "%s value is out of range.\n", name);
    return false;
  }

  if((errno == EINVAL) && (result == 0)) { // macOS does this errno
    fprintf(stderr, "The conversion for %s could not be performed.\n", name);
    return false;
  }

  if((result == 0) && *str_end) {
    // The option string doesn't contain a numerical value in the given base.
    fprintf(stderr, "Option %s value is not a number!\n", name);
    return false;
  }

  *out = result;
  return true;
}


static bool handle_size_t(const char *name, char *opt_str, size_t *out) {
  uint64_t temp;
  if(!handle_uint64_t(name, opt_str, &temp))
    return false;
  if(temp > SIZE_MAX) {
    fprintf(stderr, "Option %s value is too large!\n", name);
    return false;
  }
  *out = temp;
  return true;
}


static bool handle_double(char *name, char *opt_str, double *out) {
  char *str_end;
  double result = strtod(opt_str, &str_end);

  if(errno == ERANGE) {

    if(result == 0.0) 
      fprintf(stderr, "Option %s value has underflowed to 0!\n", name);
    else
      fprintf(stderr, "Option %s value has caused overflow!\n", name);

    return false;
    
  }

  if(*str_end != 0) {
    fprintf(stderr, "Option %s value is not a number!\n", name);
    return false;
  }

  *out = result;
  return true;
}



int cli_parse(int argc, char **argv, cli_options *options) {
  struct option longopts[] = {
    {"help", no_argument, NULL, 'h'},
    {"output", required_argument, NULL, 'o'},
    {"image-width", required_argument, NULL, 'W'},
    {"image-height", required_argument, NULL, 'H'},
    {"real-min", required_argument, NULL, 'r'},
    {"real-max", required_argument, NULL, 'R'},
    {"imag-min", required_argument, NULL, 'i'},
    {"imag-max", required_argument, NULL, 'I'},
    {"point-count", required_argument, NULL, 'p'},
    {"trials", required_argument, NULL, 't'},
    {"power", required_argument, NULL, 'P'},
    {NULL, 0, NULL, 0}
  };
  const char *shortopts = "ho:W:H:r:R:i:I:p:t:P:";

  int ch;
  while((ch = getopt_long(argc, argv, shortopts, longopts, NULL)) != -1) {
    switch(ch) {
    case 'h':
      usage();
      return -1;

    case 'o':
      options->output = optarg;
      break;

    case 'W':
      if(!handle_size_t("image-width", optarg, &options->image_width))
	return EX_USAGE;
      break;

    case 'H':
      if(!handle_size_t("image-height", optarg, &options->image_height))
	return EX_USAGE;
      break;

    case 'r':
      if(!handle_double("real-min", optarg, &options->min_real))
	return EX_USAGE;
      break;

    case 'R':
      if(!handle_double("real-max", optarg, &options->max_real))
	return EX_USAGE;
      break;

    case 'i':
      if(!handle_double("imag-min", optarg, &options->min_imag))
	return EX_USAGE;
      break;

    case 'I':
      if(!handle_double("imag-max", optarg, &options->min_imag))
	return EX_USAGE;
      break;
      
    case 'p':
      if(!handle_uint64_t("point-count", optarg, &options->max_points))
	return EX_USAGE;
      break;

    case 't':
      if(!handle_uint64_t("trials", optarg, &options->max_iterations))
	return EX_USAGE;
      break;

    case 'P':
      if(!handle_double("power", optarg, &(options->power)))
	return EX_USAGE;
      break;

    case ':':
      fputs("Missing option argument\n", stderr);
      return EX_USAGE;

    case '?':
      fputs("Unknown or ambiguous argument\n", stderr);
      return EX_USAGE;
      
    default:
      usage();
      return EX_USAGE;
    }
  }

  return 0;
}
